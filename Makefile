all: flash
.PHONY: all

## Variables
IMG := 2022-09-22-raspios-bullseye-arm64-lite.img
DEV := mmcblk0
TTY := ttyUSB0

## Flashing
URL := https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2022-09-26/$(IMG).xz
$(IMG).xz:
	wget --no-clobber "$(URL)"

$(IMG): $(IMG).xz
	unxz --keep "$(IMG).xz"

MNT := $(shell mktemp -d)
flash: $(IMG) /dev/$(DEV) $(MNT) cmdline.txt config.txt ssh.txt userconf.txt
	@ # Write image
	doas dd if="$(IMG)" of="/dev/$(DEV)" bs="1M" conv="fsync" status="progress"
	@ doas blockdev --rereadpt "/dev/$(DEV)"
	@ # Mount boot partition and write configuration
	doas mount "/dev/$(DEV)p1" "$(MNT)/"
	doas cp cmdline.txt config.txt ssh.txt userconf.txt "$(MNT)/" || true
	doas umount $(MNT)/
.PHONY: flash

## Attaching
attach:
	@ # Clear screen and hide the cursor
	@ clear && tput civis
	@ # Attach to the serial port and show the cursor again on exit
	@ trap "tput cnorm" EXIT && while ! picocom -b 115200 /dev/$(TTY); do printf "\033[0;0H"; done
.PHONY: attach
